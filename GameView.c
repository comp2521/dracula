////////////////////////////////////////////////////////////////////////
// COMP2521 18x1 ... the Fury of Dracula
// GameView.c: GameView ADT implementation
//
// 2014-07-01   v1.0    Team Dracula <cs2521@cse.unsw.edu.au>
// 2017-12-01   v1.1    Team Dracula <cs2521@cse.unsw.edu.au>

#include <assert.h>
#include <err.h>
#include <stdbool.h>
#include <stdlib.h>
#include <sysexits.h>

#include "headers/Game.h"
#include "headers/GameView.h"
#include "headers/Globals.h"
// #include "Map.h" ... if you decide to use the Map ADT

struct gameView {
    // REPLACE THIS WITH YOUR OWN IMPLEMENTATION
};

// Creates a new GameView to summarise the current state of the game
GameView
newGameView (char *pastPlays, PlayerMessage messages[])
{
    // REPLACE THIS WITH YOUR OWN IMPLEMENTATION
    GameView new = malloc (sizeof *new);
    if (new == NULL) err (EX_OSERR, "couldn't allocate GameView");

    return new;
}

// Frees all memory previously allocated for the GameView toBeDeleted
void
disposeGameView (GameView toBeDeleted)
{
    // COMPLETE THIS IMPLEMENTATION
    free (toBeDeleted);
}

//// Functions to return simple information about the current state of the game

// Get the current round
Round
getRound (GameView gv)
{
    // REPLACE THIS WITH YOUR OWN IMPLEMENTATION
    return 0;
}

// Get the id of current player - ie whose turn is it?
PlayerID
getCurrentPlayer (GameView gv)
{
    // REPLACE THIS WITH YOUR OWN IMPLEMENTATION
    return 0;
}

// Get the current score
int
getScore (GameView gv)
{
    // REPLACE THIS WITH YOUR OWN IMPLEMENTATION
    return 0;
}

// Get the current health points for a given player
int
getHealth (GameView gv, PlayerID player)
{
    // REPLACE THIS WITH YOUR OWN IMPLEMENTATION
    return 0;
}

// Get the current location id of a given player
LocationID
getLocation (GameView gv, PlayerID player)
{
    // REPLACE THIS WITH YOUR OWN IMPLEMENTATION
    return 0;
}

//// Functions that return information about the history of the game

// Fills the trail array with the location ids of the last 6 turns
void
getHistory (GameView gv, PlayerID player, LocationID trail[TRAIL_SIZE])
{
    // REPLACE THIS WITH YOUR OWN IMPLEMENTATION
}

//// Functions that query the map to find information about connectivity

// Returns an array of LocationIDs for all directly connected locations

LocationID *
connectedLocations (GameView gv, int *numLocations,
    LocationID from, PlayerID player, Round round,
    bool road, bool rail, bool sea)
{
    // REPLACE THIS WITH YOUR OWN IMPLEMENTATION
    *numLocations = 0;
    return NULL;
}
