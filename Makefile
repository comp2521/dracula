########################################################################
# COMP2521 18x1 // The Fury of Dracula // the View
#
# view/Makefile: build tests for {Game,Hunter,Drac}View

CC	= gcc
CFLAGS = -Wall -Werror -std=c99 -g

BINS	= testGameView testHunterView testDracView

all: ${BINS}

testGameView: testGameView.o GameView.o Map.o Places.o
testGameView.o: testGameView.c headers/Globals.h headers/Game.h

testHunterView: testHunterView.o HunterView.o GameView.o Map.o Places.o
testHunterView.o: testHunterView.c Map.c headers/Places.h

testDracView: testDracView.o DracView.o GameView.o Map.o Places.o
testDracView.o: testDracView.c Map.c headers/Places.h

Places.o: Places.c headers/Places.h
Map.o: Map.c headers/Map.h headers/Places.h
GameView.o: GameView.c headers/GameView.h
HunterView.o: HunterView.c headers/HunterView.h
DracView.o: DracView.c headers/DracView.h

.PHONY: clean
clean:
	-rm -f ${BINS} *.o core
